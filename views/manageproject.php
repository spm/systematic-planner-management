<?php

include "../authentication/auth.php";
include "../components/header.php";


?>




<div class="container mt-5" style="margin-bottom: 100px">

    <!-- Create form -->
    <form method="POST">
        <input type="text" class="form-control" name="project_name" placeholder="Project Name">
        <button class="btn btn-success pl-5 pr-5 mr-2 mt-2" name="new_project">Add Project</button>
    </form>


    <!-- Display any info -->

    <?php if (isset($_REQUEST['pid'])) {
        $pselection = $_REQUEST['pid'];
        if ($pselection != 'all') {
            $showProjectDetails =  allowUserToView($conn, $usern, $pselection);
        }
    } ?>

    <!-- Display Projects -->


    <form class="mt-5" method="POST" action="">
        <h5>Select Project: </h5>
        <div class="d-flex">

            <div class="mr-3 mb-3">
                <select class="select mb-3 mr-3 p-1 mt-1" style="width: 350px;" name="mpfilter" onchange="this.form.submit()">
                    <option value="all" <?php
                                        if ($pselection == 'all') {
                                            echo "selected";
                                        }

                                        ?>>ALL Projects and Tasks</option>
                    <?php foreach ($pquery as $pq) { ?>
                        <option value="<?php echo $pq['id'] ?>|<?php echo $pq['projectname'] ?>|<?php echo $pq['creater'] ?>" <?php
                                                                                                                                if ($pselection == $pq['id']) {
                                                                                                                                    $allowDeleteProject = $pq['creater'];

                                                                                                                                    echo "selected";
                                                                                                                                } ?>>ID: <?php echo $pq['id'] ?> &emsp; Project: <?php echo $pq['projectname'] ?></option><?php } ?>
                </select>
                <?php

                ?>
                <button <?php
                        if ($usern != $allowDeleteProject) {
                            echo "hidden";
                        } ?> class="btn btn-danger" name="delete_project" onclick="return confirm('Deleting the project will delete all the tasks in it too. Are you sure you want to delete it?')">Delete Selected Project</button>
            </div>

        </div>

        <div <?php if ($pselection == 'all' || $showProjectDetails == 0) {
                    echo "hidden";
                } ?>>
            <?php

            ?>
            <div class="row container mb-3">
                <strong>Project Owner: </strong><span class="card ml-1 pl-3 mr-3 bg-transparent" style="border: solid 1px white;">
                    <div class="mr-3 pt-1 pb-1">
                        <strong><?php echo $allowDeleteProject ?></strong>
                    </div>
                </span>
            </div>
            <div <?php if ($usern != $allowDeleteProject) {
                        echo "hidden";
                    } ?>>
                <h4>Add User to Project: </h4>
                <div class="row container mb-3">


                    <input type="text" placeholder="Enter name" class=" bg-white text-black text-center" name="assign" style="border-radius: 5px; width: 100px;">

                    <button class="btn btn-success btn-sm pl-3 pr-3 ml-2" name="mpadd" onclick="return confirm('Are you sure you wish to add this user?')">Add User</button>
                </div>
            </div>
            <h4>Assigned Users</h4>
            <div class="row container mt-3">

                <?php
                $manage_project_query = manageProjectQuery($conn);
                foreach ($manage_project_query as $mpq) {

                    if (($allowDeleteProject == $usern || $mpq['assignedusername'] == $usern) && $allowDeleteProject != $mpq['assignedusername']) {

                ?>
                        <div class="mr-3">
                            <input hidden type="text" name="delete_project_id" value="<?php echo $mpq['pid'] ?>">
                            <input class="btn btn-danger" type="submit" name="delete_project_assign" value="<?php echo $mpq['assignedusername'] ?> " onclick="return confirm('Are you sure you want to remove this user from your task?')">
                        </div>


                        <?php } else {
                        if ($allowDeleteProject != $usern && $allowDeleteProject != $mpq['assignedusername']) {
                        ?>
                            <div class="card pl-3 mr-3 bg-transparent" style="display:flex; flex-direction: row; border: solid 1px white;">
                                <div class="d-flex mr-3 pt-1 pb-1">
                                    <strong><?php echo $mpq['assignedusername'] ?></strong>
                                </div>
                            </div>
                <?php }
                    }
                }
                ?>

            </div>
        </div>

    </form>
</div>

<!-- Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>


</body>
<?php

include "../components/footer.php";

?>

</html>