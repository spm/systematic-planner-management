<?php

include "../authentication/auth.php";
include "../components/header.php";

?>

<!-- View Display -->
<div class="container mt-5" style="margin-bottom: 100px">

    <?php
    // [$query, $uquery, $commentQuery, $show, $id] = getTaskDataById($conn, $usern);
    foreach ($query as $q) {
        $taskID = $q['id'];
        // $usql = "SELECT * FROM assignedusers where taskID = '$taskID'";
        // $uquery = mysqli_query($conn, $usql);

        $showTask = allowUserToView($conn, $usern, $taskID);

        if ($showTask != 0) {
    ?>
            <h5>Created on <span style="color: aqua"><?php echo date("d-m-Y g:i A", strtotime($q['createdOn'])) ?></span></h5>
            <input type="text" readonly placeholder="Project" class="form-control <?php
                                                                                    if ($q['status'] == 'Completed') { ?> 
                            bg-success  text-black
                            <?php } elseif ($q['status'] == 'In_Progress') { ?> 
                                bg-warning  text-black
                            <?php } elseif ($q['status'] == 'Not_Started') { ?> 
                                bg-secondary text-white
                            <?php } ?>" style="font-weight: bold; font-size: x-large;" name="title" value="Project: <?php echo $q['title'] ?>">

            <div class="d-flex mt-2 mb-2">
                <a href="edit.php?id=<?php echo $q['id'] ?>" class="btn btn-light btn-sm" name="edit">Edit</a>
                <form method="POST" onSubmit="return confirm('Are you sure you wish to delete this task?');">
                    <input type="text" hidden value='<?php echo $q['id'] ?>' name="id">
                    <button class="btn btn-danger btn-sm ml-2" name="delete" <?php

                                                                                if ($q['creater'] != $usern) {
                                                                                    echo "hidden";
                                                                                }
                                                                                ?>>Delete</button>
                </form>
            </div>
            <div class=" p-2 rounded-lg bg-white mb-3">
                <div style="color: black"><?php echo $q['content']; ?></div>
            </div>
            <h5>Deadline: <span style="color: aqua"><?php echo date("d-m-Y g:i A", strtotime($q['deadline'])) ?></span></h5>

            <h4 class="mt-2">Created By</h4>
            <div class="btn btn-info mb-3"><?php echo $q['creater'] ?></div>
            <h4>Assigned Users</h4>
            <div class="row container mt-3">
                <?php foreach ($uquery as $uq) {
                    if ($uq['username'] != $q['creater']) {
                ?>

                        <div class="card pl-3 mr-3 bg-transparent" style="display:flex; flex-direction: row; border: solid 1px white;">
                            <div class="d-flex mr-3 pt-1 pb-1">
                                <strong><?php echo $uq['username'] ?></strong>
                            </div>
                        </div>
                <?php }
                } ?>
            </div>
            <h4 class="mt-3">Comments:</h4>
            <div class="column container mt-1">

                <?php foreach ($commentQuery as $cq) {
                    if ($usern != $cq['creater']) {
                ?>

                        <div class="card mt-1 pl-3 mr-3 bg-white" style=" border: solid 1px white; color: black;">
                            <div class="mr-3 pt-1 pb-1">
                                <strong class="border-bottom border-dark text-center mr-1" style="color: red; width: 100px;"><?php echo $cq['creater'] ?></strong>
                                <span>&nbsp;<?php echo $cq['comment'] ?></span>
                            </div>
                        </div>
                    <?php } else {
                    ?>

                        <div class="card mt-1 pl-3 mr-3 bg-white" style=" border: solid 1px white; color: black;">
                            <div class="mr-3 pt-1 pb-1">
                                <strong class="border-bottom border-dark text-center mr-1" style="color: green; width: 100px;"><?php echo $cq['creater'] ?></strong>
                                <span><?php echo $cq['comment'] ?></span>
                                <form method="POST">
                                    <input hidden type="text" name="deletecommentid" value="<?php echo $cq['id'] ?>">
                                    <button class="btn btn-danger pl-5 pr-5 mr-2 mt-2" name="delete_comment">Delete Comment</button>
                                </form>
                            </div>
                        </div>
                <?php }
                } ?>
            </div>

            <!-- Create comment -->

            <form class="mt-2" method="POST">
                <input hidden type="text" name="deletetaskid" value="<?php echo $taskID ?>">
                <!-- <input type="text" class="form-control" name="comment" placeholder="Comment ..."> -->
                <div class="mt-2 mb-2">
                    <textarea id="mytextareacomment" placeholder="Comment ..." name="comment"></textarea>
                </div>
                <button class="btn btn-success pl-5 pr-5 mr-2 mt-2" name="add_comment">Add Comment</button>
            </form>


            </form>
        <?php } else {
        ?><h1>NO ACCESS: Return to Home Page</h1>
    <?php    }
    } ?>


</div>

<!-- Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>


</body>
<?php

include "../components/footer.php";

?>