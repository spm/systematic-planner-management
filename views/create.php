<?php

include "../authentication/auth.php";
include "../components/header.php";


?>



<div class="container mt-5" style="margin-bottom: 100px">
    <?php

    if (isset($_REQUEST['projectId'])) {
        $projectId = $_REQUEST['projectId'];
        $showProject = allowUserToView($conn, $usern, $projectId);
    } else {
        $showProject = 1;
    }
    if (isset($_REQUEST['noproject'])) { ?>
        <div class="alert alert-warning d-flex" style="justify-content: space-between;" role="alert">
            <div>Select a Project</div>
            <a class="btn btn-sm mr-2" style="border: solid 1px black;" href="create.php">X</a>
        </div>
    <?php }

    ?>

    <?php
    if ($showProject != 0) {
    ?>
        <!-- Display any info -->
        <form method="POST" class="mt-5">
            <div class="d-flex">
                <h5>Select Project: </h5>
                <select class="select mb-3 ml-3 p-1 " style="width: 350px;" name="p_and_id">
                    <option value="Select_Project|Select_Project" selected>Select A Project</option>

                    <?php foreach ($pquery as $pq) { ?>

                        <option value="<?php echo $pq['id'] ?>|<?php echo $pq['projectname'] ?>" <?php if ($pq['id'] == $projectId) {
                                                                                                        echo "selected";
                                                                                                    } ?>>ID: <?php echo $pq['id'] ?> &emsp; Project: <?php echo $pq['projectname'] ?> </option>
                    <?php } ?>
                </select>
            </div>

            <div class="mb-3">
                <textarea id="mytextarea" name="content"><?php echo $q['content'] ?></textarea>
            </div>
            <strong>Deadline: </strong>
            <input class="deadline" name="deadline" type="datetime-local" min="<?php echo date('Y-m-d\TH:i', strtotime('-6 hours')) ?>" max="3000-01-01" required>
            <br>
            <br>
            <button class="btn btn-success pl-5 pr-5 mr-2" name="new_task">Add Task</button>


        </form>
    <?php } else {
    ?><h1>NO ACCESS: Return to Home Page</h1>
    <?php    }
    ?>
</div>

<!-- Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>


</body>

<?php

include "../components/footer.php";

?>