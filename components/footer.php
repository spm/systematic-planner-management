<?php if (basename($_SERVER['PHP_SELF']) == 'footer.php') {
	header("location: index.php");
} ?>

<!-- Footer -->
<footer class="navbar fixed-bottom container mt-auto py-3 text-white" style="justify-content: space-between; border-radius: 5px 5px 0px 0px; display: grid 1fr 1fr 1fr; background-color:rgba(0, 0, 0, 0.3);">
	<div class="text-left">Developed by Team <span style="color: aqua;"> SPM </span></div>

	<div class="text-center">Copyright <?php echo date("Y"); ?> @ Sarthak Kahaliya, Priyanshi Sharma, Sarthak Vinayaka, Mehul Bhanushali</div>

	<div class="text-right">
		<a target='_blank' href="" style="color: aqua; margin-right: 20px; ">Github <i class="fa-brands fa-github"></i></a>

	</div>


</footer>

</html>