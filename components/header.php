<?php if (basename($_SERVER['PHP_SELF']) == 'header.php') {
  header("location: index.php");
} ?>

<?php
if (basename($_SERVER['PHP_SELF']) == 'index.php') {
  include "logic/logic.php";
} else {
  include "../logic/logic.php";
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <script src="https://kit.fontawesome.com/aa2d2822a6.js" crossorigin="anonymous"></script>
  <!-- <script src="https://cdn.ckeditor.com/4.21.0/standard/ckeditor.js"></script> -->
  <?php if (basename($_SERVER['PHP_SELF']) == 'index.php') { ?>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">
    <link rel="manifest" href="assets/site.webmanifest">
  <?php } else { ?>
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="apple-touch-icon" sizes="180x180" href="../assets/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/img/favicon-16x16.png">
    <link rel="manifest" href="../assets/site.webmanifest">
  <?php
  } ?>
  <!-- <script src="https://cdn.tiny.cloud/1/qqg2o60ayqselv6gpbwtzkkf7pehsiu8218v829ahayjubv0/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script> -->
  <script src="https://cdn.tiny.cloud/1/qqg2o60ayqselv6gpbwtzkkf7pehsiu8218v829ahayjubv0/tinymce/4/tinymce.min.js"></script>

  <script>
    tinymce.init({
      selector: '#mytextarea',
      height: 300,
      plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
      toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | addcomment showcomments | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
    });

    tinymce.init({
      selector: '#mytextareacomment',
      height: 10,
      plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
      toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | addcomment showcomments | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
    });
  </script>



  <title>Systematic Planner & Management</title>
</head>



<body>

  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 1600 900" preserveAspectRatio="xMidYMax slice">
    <!-- <h1>How are you</h1> -->
    <defs>
      <linearGradient id="bg">
        <stop offset="0%" style="stop-color:rgba(130, 158, 249, 0.06)"></stop>
        <stop offset="50%" style="stop-color:rgba(76, 190, 255, 0.6)"></stop>
        <stop offset="100%" style="stop-color:rgba(115, 209, 72, 0.2)"></stop>
      </linearGradient>
      <path id="wave" fill="url(#bg)" d="M-363.852,502.589c0,0,236.988-41.997,505.475,0
	s371.981,38.998,575.971,0s293.985-39.278,505.474,5.859s493.475,48.368,716.963-4.995v560.106H-363.852V502.589z" />
    </defs>
    <g>
      <use xlink:href='#wave' opacity=".3">
        <animateTransform attributeName="transform" attributeType="XML" type="translate" dur="10s" calcMode="spline" values="270 230; -334 180; 270 230" keyTimes="0; .5; 1" keySplines="0.42, 0, 0.58, 1.0;0.42, 0, 0.58, 1.0" repeatCount="indefinite" />
      </use>
      <use xlink:href='#wave' opacity=".6">
        <animateTransform attributeName="transform" attributeType="XML" type="translate" dur="8s" calcMode="spline" values="-270 230;243 220;-270 230" keyTimes="0; .6; 1" keySplines="0.42, 0, 0.58, 1.0;0.42, 0, 0.58, 1.0" repeatCount="indefinite" />
      </use>
      <use xlink:href='#wave' opacty=".9">
        <animateTransform attributeName="transform" attributeType="XML" type="translate" dur="6s" calcMode="spline" values="0 230;-140 200;0 230" keyTimes="0; .4; 1" keySplines="0.42, 0, 0.58, 1.0;0.42, 0, 0.58, 1.0" repeatCount="indefinite" />
      </use>
    </g>

  </svg>

  <div class="container">
    <hgroup>
      <h1 class="site-title mt-2 " style="text-align: center;"><strong>Systematic Planner & Management</strong></h1>
    </hgroup>


    <!-- Login Page -->
    <h3 class="mt-5 pt-5" style="color: white; padding-left: 0px; padding-right: 0px; margin-top: 0px; text-align: center;" <?php if (basename($_SERVER['PHP_SELF']) != 'login.php') {
                                                                                                                              echo "hidden";
                                                                                                                            }  ?>>Login</h3>


    <!-- Signup Page -->
    <h3 class="mt-5 pt-5" style="color: white; padding-left: 0px; padding-right: 0px; margin-top: 0px; text-align: center;" <?php if (basename($_SERVER['PHP_SELF']) != 'register.php') {
                                                                                                                              echo "hidden";
                                                                                                                            }  ?>>Sign Up</h3>


    <!-- Navigation Bar -->
    <nav class="navbar navbar-dark  mt-3 pt-3" style="border-radius: 5px" <?php if (basename($_SERVER['PHP_SELF']) == 'login.php') {
                                                                            echo "hidden";
                                                                          }  ?>>

      <div style="display: flex; flex-direction: row; justify-content: space-between; align-items: start;">

        <!-- for View page -->
        <div style="color: white; padding-left: 0px; padding-right: 0px; margin-top: 0px;" <?php if (basename($_SERVER['PHP_SELF']) != 'view.php') {
                                                                                              echo "hidden";
                                                                                            }     ?>>

        </div>

        <!-- for index page -->
        <div style="color: white; padding-left: 0px; padding-right: 0px; margin-top: 0px;" <?php if (basename($_SERVER['PHP_SELF']) != 'index.php') {
                                                                                              echo "hidden";
                                                                                            }     ?>>
          <h3>Welcome <?php echo $_SESSION['username']; ?>!</h3>
        </div>


        <!-- for create page -->
        <div style="color: white; padding-left: 0px; padding-right: 0px; margin-top: 0px;" <?php if (basename($_SERVER['PHP_SELF']) != 'create.php') {
                                                                                              echo "hidden";
                                                                                            }     ?>>
          <h3>Create Task</h3>
        </div>

        <!-- for edit page -->
        <div style="color: white; padding-left: 0px; padding-right: 0px; margin-top: 0px;" <?php if (basename($_SERVER['PHP_SELF']) != 'edit.php') {
                                                                                              echo "hidden";
                                                                                            }     ?>>
          <h3>Edit Task</h3>
        </div>

      </div>


      <!-- Nav bar login and signup button -->
      <div <?php if (basename($_SERVER['PHP_SELF']) == 'login.php' || basename($_SERVER['PHP_SELF']) == 'register.php') {
              echo "hidden";
            }     ?>>


        <a <?php if (basename($_SERVER['PHP_SELF']) == 'index.php') { ?> href="index.php" <?php } else { ?> href="../index.php" <?php
                                                                                                                              } ?> class="btn btn-outline-light my-3">Home</a>

        <a class="logout btn btn-danger " <?php if (basename($_SERVER['PHP_SELF']) == 'index.php') { ?> href="authentication/logout.php" <?php } else { ?> href="../authentication/logout.php" <?php
                                                                                                                                                                                              } ?>>Logout</a>



      </div>

    </nav>
  </div>