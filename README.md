## Systematic Planner and Management

<br/>
Created By: [Sarthak Kahaliya](https://github.com/SarthakKahaliya) &nbsp; [Priyanshi Sharma](https://github.com/priyanshi2106) &nbsp; [Sarthak Vinayaka](https://github.com/sarthakvinayaka) &nbsp; [Mehul Bhanushali](https://github.com/mehulbvt)
<br/>
Team - SPM

## ABOUT THE PROJECT

Systematic planning and management tool helps individual users, teams, and enterprises plan, assign, track, and manage work, bringing teams together for everything from trip planning to ToDo lists to wedding planning and even software development.
The system can be used by individuals or teams to manage their work and stay organized. The project would involve designing and developing a user-friendly interface that allows users to easily create and view tasks, assign them to team members, set deadlines, and track progress. Finally, the system would be built with security in mind and would provide users with the ability to control access to their tasks and projects.
