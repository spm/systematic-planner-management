<?php

// Initial Function for global variable

use PHPMailer\PHPMailer\PHPMailer;

function initializeGlobal($conn, $usern)
{

    $sql = "SELECT * FROM taskdata WHERE id IN(SELECT taskID from assignedusers WHERE username = '$usern') ORDER BY createdOn DESC";
    $query = mysqli_query($conn, $sql);

    $selection = "createddss";
    $pselection = "all";


    // Get Project Data

    // $sql = "SELECT * FROM projectdata";
    // $pquery = mysqli_query($conn, $sql);

    $sql = "SELECT * FROM projectdata WHERE id IN(SELECT pid from projectusers WHERE assignedusername = '$usern')";
    $pquery = mysqli_query($conn, $sql);


    return array($query, $selection, $pselection, $pquery);
}

//Sorting Project Listed on index based on project and order by deadline and created on ascending and descending
function sortProject($conn, $usern)
{
    $selection = $_REQUEST['orderby'];
    $pselection = $_REQUEST['pfilter'];

    if ($pselection == "all") {

        if ($selection == 'deadline') {
            $sql = "SELECT * FROM taskdata WHERE id IN(SELECT taskID from assignedusers WHERE username = '$usern') ORDER BY deadline ASC";
            $query = mysqli_query($conn, $sql);

            $selection = "deadline";
        }
        if ($selection == 'createdass') {
            $sql = "SELECT * FROM taskdata WHERE id IN(SELECT taskID from assignedusers WHERE username = '$usern') ORDER BY createdOn ASC";
            $query = mysqli_query($conn, $sql);

            $selection = "createdass";
        }

        if ($selection == 'createddss') {
            $sql = "SELECT * FROM taskdata WHERE id IN(SELECT taskID from assignedusers WHERE username = '$usern') ORDER BY createdOn DESC";
            $query = mysqli_query($conn, $sql);

            $selection = "createddss";
        }
    } else {

        $p_and_id = explode('|', $_REQUEST['pfilter']);
        $pid = $p_and_id[0];
        $title = $p_and_id[1];


        if ($selection == 'deadline') {
            $sql = "SELECT * FROM taskdata WHERE id IN(SELECT taskID from assignedusers WHERE username = '$usern') AND pid = $pid ORDER BY deadline ASC";
            $query = mysqli_query($conn, $sql);

            $selection = "deadline";
            $pselection = $pid;
        }
        if ($selection == 'createdass') {
            $sql = "SELECT * FROM taskdata WHERE id IN(SELECT taskID from assignedusers WHERE username = '$usern') AND pid = $pid ORDER BY createdOn ASC";
            $query = mysqli_query($conn, $sql);

            $selection = "createdass";
            $pselection = $pid;
        }

        if ($selection == 'createddss') {
            $sql = "SELECT * FROM taskdata WHERE id IN(SELECT taskID from assignedusers WHERE username = '$usern') AND pid = $pid ORDER BY createdOn DESC";
            $query = mysqli_query($conn, $sql);

            $selection = "createddss";
            $pselection = $pid;
        }
    }

    return array($query, $selection, $pselection);
}

// Project Filter
function projectFilter($conn)
{
    $pselection = $_REQUEST['mpfilter'];

    if ($pselection != "all") {

        $p_and_id = explode('|', $_REQUEST['mpfilter']);
        $pid = $p_and_id[0];
        // $title = $p_and_id[1];

        $pselection = $pid;
    }
    header("Location: manageproject.php?pid=$pselection");
}

// Manage Project Query
function manageProjectQuery($conn)
{
    $pid = $_REQUEST['pid'];
    $sql = "SELECT * FROM projectusers WHERE pid = $pid";
    $manage_project_query = mysqli_query($conn, $sql);



    return $manage_project_query;
}

// Assigning User to a project
function assignUserToProject($conn)
{
    $pselection = $_REQUEST['mpfilter'];
    $p_and_id = explode('|', $_REQUEST['mpfilter']);
    $pid = $p_and_id[0];
    $assign = $_REQUEST['assign'];

    //echo "$pid, $assign";
    if ($assign != '') {
        $sql = "SELECT * FROM users WHERE  username='$assign'";
        $result = mysqli_query($conn, $sql);
        if ($result) {
            if (mysqli_num_rows($result) >= 1) {
                $sql = "SELECT * FROM projectusers WHERE pid = $pid AND assignedusername = '$assign'";
                $exist = mysqli_query($conn, $sql);
                if ($exist) {
                    if (mysqli_num_rows($exist) >= 1) {

                        $pselection = $pid;
                    } else {
                        $sql = "INSERT INTO projectusers(pid,assignedusername) VALUES($pid, '$assign')";
                        mysqli_query($conn, $sql);

                        $pselection = $pid;
                    }
                } else {

                    $pselection = $pid;
                }
                header("Location: manageproject.php?pid=$pselection");
            }
        }
    }
}

// Remove User from a Project
function removeUserFromProject($conn)
{
    $pid = $_REQUEST['delete_project_id'];
    $assignedusername = $_REQUEST['delete_project_assign'];

    $sql = "DELETE FROM projectusers WHERE pid = $pid and assignedusername = '$assignedusername'";
    mysqli_query($conn, $sql);

    $sql = "DELETE FROM assignedusers WHERE username = '$assignedusername' and taskID IN(SELECT id FROM taskdata WHERE pid = '$pid')";
    mysqli_query($conn, $sql);

    $sql = "DELETE FROM taskdata WHERE creater = '$assignedusername' and pid = '$pid'";
    mysqli_query($conn, $sql);

    $pselection = $pid;
    header("Location: manageproject.php?pid=$pselection");
}

function sendMail($conn, $taskID, $assignedUser)
{
    // Create a new PHPMailer instance
    $mail = new PHPMailer(true);

    // Set up SMTP configuration
    $mail->isSMTP();
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';
    $mail->SMTPAuth = true;
    $mail->Username = 'capstonevt23@gmail.com'; // your Gmail email address
    $mail->Password = 'yxngejkatpjaxhbn'; // your Gmail password

    // Set up email content
    $mail->setFrom('capstonevt23@gmail.com', 'SPM'); // Sender name and email address

    $sql = "SELECT * FROM taskdata WHERE id = $taskID";
    $taskdatas = mysqli_query($conn, $sql);

    foreach ($taskdatas as $taskdata) {
        $title = $taskdata['title'];
        $content = $taskdata['content'];
        $creater = $taskdata['creater'];
        $sql = "SELECT * FROM users WHERE username = '$assignedUser'";
        $userdatas = mysqli_query($conn, $sql);
        foreach ($userdatas as $userdata) {
            $to = $userdata['email'];
            $mail->addAddress($to, $assignedUser); // Recipient name and email address
            $mail->Subject = 'Assigned to ' . $title . ' by ' . $creater; // email subject
            // $mail->Body = 'This is a test email sent using PHPMailer and Gmail SMTP.'; // email body
            $mail->isHTML(true); // Set email body as HTML
            $mail->msgHTML($content); // email body in HTML format


            // Send the email and check for errors
            if ($mail->send()) {
                echo 'Email sent successfully.';
            } else {
                echo 'Error: ' . $mail->ErrorInfo;
            }
        }
    }
}


// Create a Project
function createProject($conn, $usern)
{
    $project_name = strip_tags($_REQUEST['project_name']);

    $sql = "INSERT INTO projectdata(projectname, creater) VALUES('$project_name', '$usern')";
    mysqli_query($conn, $sql);
    $project_created_id = mysqli_insert_id($conn);

    // adding the creator to the assigned users for the project
    $sql = "INSERT INTO projectusers(pid, assignedusername) VALUES('$project_created_id', '$usern')";
    if (mysqli_query($conn, $sql)) {
        // Pass in project ID, Project Name, usern
        // sendMail();
    }

    header("Location: create.php?info=addedproject&projectId=$project_created_id");
    exit();
}

// Create a Task
function createTask($conn, $usern)
{
    $p_and_id = explode('|', $_REQUEST['p_and_id']);
    $pid = $p_and_id[0];
    if ($pid == "Select_Project") {
        header("Location: create.php?noproject=select_project");
        exit();
    }
    $title = $p_and_id[1];
    $content = $_REQUEST['content'];
    $deadline = $_REQUEST['deadline'];
    $createdOn = date('Y-m-d\TH:i', strtotime('-6 hours'));

    $sql = "INSERT INTO taskdata(pid, title, content, deadline, status, createdOn , creater) VALUES('$pid', '$title', '$content', '$deadline', 'Not_Started', '$createdOn', '$usern')";

    if (mysqli_query($conn, $sql)) {

        $last_id = mysqli_insert_id($conn);
        $sql = "INSERT INTO assignedusers(taskID, username) VALUES('$last_id', '$usern')";
        if (mysqli_query($conn, $sql)) {
            sendMail($conn, $last_id, $usern);
        }

        header("Location: ../views/edit.php?id=$last_id");
        exit();
    }
}

// Create a Comment
function addComment($conn, $usern)
{
    $id = $_REQUEST['id'];
    $comment = $_REQUEST['comment'];

    $sql = "INSERT INTO taskComments(taskID, comment, creater) VALUES('$id', '$comment', '$usern')";
    mysqli_query($conn, $sql);

    header("Location: view.php?id=$id");
    exit();
}

// Delete a Comment
function deleteComment($conn, $usern)
{
    $commentID = $_REQUEST['deletecommentid'];
    $id = $_REQUEST['id'];
    $sql = "DELETE FROM taskComments WHERE id = $commentID and creater = '$usern'";
    mysqli_query($conn, $sql);
    header("Location: view.php?id=$id");
    exit();
}

// Get Task Data Based on ID
function getTaskDataById($conn)
{
    $id = $_REQUEST['id'];
    $sql = "SELECT * FROM taskdata WHERE id = $id";
    $query = mysqli_query($conn, $sql);
    $usql = "SELECT * FROM assignedusers where taskID = '$id'";
    $uquery = mysqli_query($conn, $usql);
    $commentQuerySql = "SELECT * FROM taskComments where taskID = '$id'";
    $commentQuery = mysqli_query($conn, $commentQuerySql);

    return array($query, $uquery, $commentQuery, $id);
}

// Delete Assigned User
function deleteAssignedUserFromTask($conn, $deleteUserType)
{
    $id = $_REQUEST['deletetaskid'];
    $username = $_REQUEST[$deleteUserType];



    $sql = "DELETE FROM assignedusers WHERE taskID = $id and username = '$username'";
    mysqli_query($conn, $sql);

    if ($deleteUserType == 'deleteassign') {
        header("Location: edit.php?info=removed&id=$id&user=$username");
        exit();
    } else {
        header("Location: ../index.php");
        exit();
    }
}

// Delete a Task
function deleteTask($conn, $id, $redirection)
{
    // echo "$id";

    $sql = "SELECT creater from taskdata WHERE id = $id";
    $creater = mysqli_query($conn, $sql);

    $sql = "DELETE FROM taskdata WHERE id = $id";
    mysqli_query($conn, $sql);


    // Deleting all the users assigned to the task

    $sql = "DELETE FROM assignedusers WHERE taskID = $id";
    mysqli_query($conn, $sql);


    // Deleting all the comments of the task:

    $sql = "DELETE FROM taskComments WHERE taskID = $id";
    mysqli_query($conn, $sql);

    if ($redirection == True) {


        if (basename($_SERVER['PHP_SELF']) == 'index.php') {
            header("Location: index.php?info=deleted");
            exit();
        } else {
            header("Location: ../index.php?info=deleted");
            exit();
        }
    }
}

// Update a task
function updateTask($conn)
{
    $id = $_REQUEST['id'];
    $content = $_REQUEST['content'];
    $status = $_REQUEST['status'];
    $deadline = $_REQUEST['deadline'];


    $sql = "UPDATE taskdata SET content = '$content', status = '$status', deadline = '$deadline' WHERE id = $id";
    mysqli_query($conn, $sql);

    header("Location: ../index.php?info=updated");
    exit();
}

// Assigning a User to Task and Project
function assignUserToTaskAndProject($conn)
{
    $id = $_REQUEST['id'];
    $content = $_REQUEST['content'];
    $status = $_REQUEST['status'];
    $assign = strip_tags($_REQUEST['assign']);
    $deadline = $_REQUEST['deadline'];
    if ($assign != '') {
        $username = mysqli_real_escape_string($conn, $assign);
        $sql = "SELECT * FROM users WHERE  username='$username'";
        $result = mysqli_query($conn, $sql);

        if ($result) {
            if (mysqli_num_rows($result) >= 1) {

                $sql = "SELECT * FROM assignedusers WHERE taskID = $id AND username = '$username'";
                $exist = mysqli_query($conn, $sql);
                if ($exist) {
                    if (mysqli_num_rows($exist) >= 1) {
                        header("Location: edit.php?info=exist&id=$id&user=$username");
                        exit();
                    } else {

                        $sql = "UPDATE taskdata SET content = '$content', status = '$status', deadline = '$deadline' WHERE id = $id";
                        if (mysqli_query($conn, $sql)) {
                            $sql = "INSERT INTO assignedusers(taskID, username) VALUES('$id', '$username')";
                            if (mysqli_query($conn, $sql)) {
                                sendMail($conn, $id, $username);
                            }



                            $current_pid = mysqli_query($conn, "SELECT * FROM taskdata WHERE id = $id");

                            foreach ($current_pid as $cpid) {

                                $sql = "SELECT * FROM projectusers WHERE pid = $cpid[pid] AND assignedusername = '$username'";
                                $exist = mysqli_query($conn, $sql);
                                if ($exist) {
                                    if (mysqli_num_rows($exist) == 0) {
                                        $sql = "INSERT INTO projectusers(pid,assignedusername) VALUES($cpid[pid], '$username')";
                                        mysqli_query($conn, $sql);
                                    }
                                }
                            }


                            header("Location: edit.php?info=added&id=$id&user=$username");
                            exit();
                        }
                    }
                }
            } else {
                header("Location: edit.php?info=usernotfound&id=$id&user=$username");
                exit();
            }
        }
    } else {
        $sql = "UPDATE taskdata SET content = '$content', status = '$status' WHERE id = $id";
        mysqli_query($conn, $sql);

        header("Location: ../index.php?info=updated");
        exit();
    }
}

// Delete Project and related task and assigned users
function deleteProjectAndItsTasksAndItsUsers($conn, $usern, $filterType, $redirection)
{
    if ($_REQUEST[$filterType] != 'all') {
        $p_and_id = explode('|', $_REQUEST[$filterType]);
        $pid = $p_and_id[0];
        $title = $p_and_id[1];

        $creater = $p_and_id[2];

        // echo "$creater, $usern";

        if ($creater == $usern) {


            $sql = "SELECT * FROM taskdata WHERE pid = $pid";
            $query = mysqli_query($conn, $sql);

            foreach ($query as $q) {



                // removing assigned users from task
                $qtaskid = $q['id'];

                deleteTask($conn, $qtaskid, False);
                // $sql = "DELETE FROM assignedusers WHERE taskID = $qtaskid ";
                // mysqli_query($conn, $sql);

                // // Deleting all the comments of the task:

                // $sql = "DELETE FROM taskComments WHERE taskID = $qtaskid";
                // mysqli_query($conn, $sql);
            }


            // $sql = "DELETE FROM taskdata WHERE pid = $pid";
            // mysqli_query($conn, $sql);

            $sql = "DELETE FROM projectdata WHERE id = $pid";
            mysqli_query($conn, $sql);

            $sql = "DELETE FROM projectusers WHERE pid = $pid";
            mysqli_query($conn, $sql);

            header("Location: " . $redirection . ".php");
            exit();
        } else {
            header("Location: " . $redirection . ".php?info=notprojectcreater");
            exit();
        }
    }
}

function allowUserToView($conn, $usern, $id)
{
    $show = 0;
    if (basename($_SERVER['PHP_SELF']) == 'view.php' || basename($_SERVER['PHP_SELF']) == 'edit.php') {
        $sql = "SELECT * FROM assignedusers WHERE taskID = $id AND username = '$usern'";
        $exist = mysqli_query($conn, $sql);
        if ($exist) {
            if (mysqli_num_rows($exist) == 1) {
                $show = 1;
            } else {
                $show = 0;
            }
        }
    } else {
        $sql = "SELECT * FROM projectusers WHERE pid = $id AND assignedusername = '$usern'";
        $exist = mysqli_query($conn, $sql);
        if ($exist) {
            if (mysqli_num_rows($exist) == 1) {
                $show = 1;
            } else {
                $show = 0;
            }
        }
    }

    return $show;
}
