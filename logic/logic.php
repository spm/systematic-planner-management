
<?php

if (basename($_SERVER['PHP_SELF']) == 'logic.php') {
    header("location: ../index.php");
}

// Don't display server errors 
ini_set("display_errors", "off");

if (basename($_SERVER['PHP_SELF']) == 'index.php') {
    include "database/db.php";
} else {
    include "../database/db.php";
}

// Destroy if not possible to create a connection
if (!$conn) {
    echo "<h3 class='container bg-dark p-3 text-center text-warning rounded-lg mt-5'>Not able to establish Database Connection<h3>";
}

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Setting up PHPMailer Package
if (basename($_SERVER['PHP_SELF']) == 'index.php') {
    require_once "logic/PHPMailer/Exception.php";
    require_once "logic/PHPMailer/PHPMailer.php";
    require_once "logic/PHPMailer/SMTP.php";
} else {
    require_once "../logic/PHPMailer/Exception.php";
    require_once "../logic/PHPMailer/PHPMailer.php";
    require_once "../logic/PHPMailer/SMTP.php";
}


// Get data to display on index page
if (basename($_SERVER['PHP_SELF']) == 'index.php') {
    include "logic/methods.php";
} else {
    include "../logic/methods.php";
}

// Initializing Global variables and first queries
[$query, $selection, $pselection, $pquery] = initializeGlobal($conn, $usern);

//Sorting Project Listed on index based on project and order by deadline and created on ascending and descending
if (isset($_REQUEST['pfilter']) || isset($_REQUEST['orderby'])) {
    [$query, $selection, $pselection] = sortProject($conn, $usern);
}

// Project Filter
if (isset($_REQUEST['mpfilter'])) {
    projectFilter($conn);
}

// Assign user to a project.
if (isset($_REQUEST['mpadd'])) {
    assignUserToProject($conn);
}

// Remove User from a project
if (isset($_REQUEST['delete_project_assign'])) {
    removeUserFromProject($conn);
    //$pselection = $_REQUEST['pid'];
}

// Create a new project
if (isset($_REQUEST['new_project'])) {
    createProject($conn, $usern);
}

// Create a new task
if (isset($_REQUEST['new_task'])) {
    createTask($conn, $usern);
}

// Get task data based on id
if (isset($_REQUEST['id'])) {
    [$query, $uquery, $commentQuery, $id] = getTaskDataById($conn);
}

// Delete a task
if (isset($_REQUEST['delete'])) {
    $id = $_REQUEST['id'];
    deleteTask($conn, $id, True);
}

// Update a task
if (isset($_REQUEST['update'])) {
    updateTask($conn);
}

// Assigning User to Task and Project
if (isset($_REQUEST['adduser'])) {
    assignUserToTaskAndProject($conn);
}

// Delete Assigned User
if (isset($_REQUEST['deleteassign'])) {
    $deleteUserType = 'deleteassign';
    deleteAssignedUserFromTask($conn, $deleteUserType);
}

// Deleting only one specific assigned user who is not the creater
if (isset($_REQUEST['deleteonlythis'])) {
    $deleteUserType = 'deleteonlythis';
    deleteAssignedUserFromTask($conn, $deleteUserType);
}

// Delete Project and related task and assigned users
if (isset($_REQUEST['delete_project'])) {
    if (basename($_SERVER['PHP_SELF']) == 'manageproject.php') {
        $filterType = 'mpfilter';
        $redirection = 'manageproject';
    } else {
        $filterType = 'pfilter';
        $redirection = 'index';
    }
    deleteProjectAndItsTasksAndItsUsers($conn, $usern, $filterType, $redirection);
}

// Commenting on the Task
if (isset($_REQUEST['add_comment'])) {
    addComment($conn, $usern);
}

// Deleting comment on the task
if (isset($_REQUEST['delete_comment'])) {
    deleteComment($conn, $usern);
}

?>
