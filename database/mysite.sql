-- database file
/**
This table contains the information about the users that are assigned task
**/
CREATE TABLE `assignedusers` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `taskID` int(11) NOT NULL,
 `username` varchar(128) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

/**
This table contains the information about the project 
**/
CREATE TABLE `projectdata` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `projectname` varchar(128) NOT NULL,
 `creater` varchar(128) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;



/**
This table contains list of the users related to a specific project
**/
CREATE TABLE `projectusers` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `pid` int(11) NOT NULL,
 `assignedusername` varchar(128) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


/**
This table contains the information about the task
**/
CREATE TABLE `taskdata` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `pid` int(11) NOT NULL,
 `title` varchar(128) NOT NULL,
 `content` longtext NOT NULL,
 `deadline` datetime NOT NULL,
 `status` varchar(20) NOT NULL,
 `createdOn` timestamp NOT NULL DEFAULT current_timestamp(),
 `creater` varchar(128) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;



/**
This table contains the comments related to a specific task
**/
CREATE TABLE `taskComments` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `taskID` int(11) NOT NULL,
 `comment` longtext NOT NULL,
 `createdOn` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
 `creater` varchar(128) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


/**
This table contains the information about users registered on the website
**/
CREATE TABLE `users` (
 `userid` int(11) NOT NULL AUTO_INCREMENT,
 `username` varchar(150) NOT NULL,
 `email` varchar(100) NOT NULL,
 `password` varchar(100) NOT NULL,
 `isEmailVerified` tinyint(4) NOT NULL,
 `token` varchar(10) NOT NULL,
 PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;