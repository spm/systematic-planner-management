<?php

include "../database/db.php";

if (!isset($_GET['email']) || !isset($_GET['token'])) {
    header('Location: register.php');
    exit();
} else {

    $email = $_GET['email'];
    $token = $_GET['token'];
    $sql = "UPDATE users SET isEmailVerified=1, token='' WHERE email = '$email' AND token = '$token' AND isEmailVerified = 0";

    $result = mysqli_query($conn, $sql);

    if ($result) {
        header('Location: login.php?verified=true');
        exit();
    } else {
        header('Location: register.php');
        exit();
    }
}
