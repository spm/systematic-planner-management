<?php


session_start();


include "../database/db.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once "../logic/PHPMailer/Exception.php";
require_once "../logic/PHPMailer/PHPMailer.php";
require_once "../logic/PHPMailer/SMTP.php";


if (isset($_POST['register_btn'])) {
  $username = mysqli_real_escape_string($conn, strip_tags($_POST['username']));
  $email = mysqli_real_escape_string($conn, strip_tags($_POST['email']));
  $password = mysqli_real_escape_string($conn, strip_tags($_POST['password']));
  $password2 = mysqli_real_escape_string($conn, strip_tags($_POST['password2']));
  $query = "SELECT * FROM users WHERE username = '$username'";
  $result = mysqli_query($conn, $query);
  if ($result) {

    if (mysqli_num_rows($result) > 0) {
      $_SESSION['message'] = "Username already exists";
    } else {
      $query = "SELECT * FROM users WHERE email = '$email'";
      $result = mysqli_query($conn, $query);
      if ($result) {
        if (mysqli_num_rows($result) > 0) {
          $_SESSION['message'] = "Email already exists";
        } else {

          if ($password == $password2) {           //Create User
            $passwordHash = md5($password); //hash password before storing for security purposes
            $token = "qwertyuiopasdfghjkl;zxcvbnm,.1234567890-=%+_()*!asdfjknagoerngoiqhgihewqoifhpoihpioehfvoeidng3r769037514509184375";
            $token = str_shuffle($token);
            $token = substr($token, 0, 10);
            $sql = "INSERT INTO users(username, email, password, isEmailVerified, token ) VALUES('$username', '$email', '$passwordHash', '0', '$token')";
            if (mysqli_query($conn, $sql)) {
              $mail = new PHPMailer(true);
              // Set up SMTP configuration
              $mail->isSMTP();
              $mail->Host = 'smtp.gmail.com';
              $mail->Port = 587;
              $mail->SMTPSecure = 'tls';
              $mail->SMTPAuth = true;
              $mail->Username = 'capstonevt23@gmail.com'; // your Gmail email address
              $mail->Password = 'yxngejkatpjaxhbn'; // your Gmail password
              // Set up email content
              $mail->setFrom('capstonevt23@gmail.com', 'SPM'); // Sender name and email address
              $mail->addAddress($email, $username); // Recipient name and email address
              $mail->Subject = 'Email Verification'; // email subject
              // $mail->Body = 'This is a test email sent using PHPMailer and Gmail SMTP.'; // email body
              $mail->isHTML(true); // Set email body as HTML
              $content = "
              <!DOCTYPE html>
              <html lang='en'>
              <head>
                <meta charset='UTF-8'>
                <meta name='viewport' content='width=device-width,initial-scale=1'>
                <meta name='x-apple-disable-message-reformatting'>
                <title></title>
                <!--[if mso]>
                <noscript>
                  <xml>
                    <o:OfficeDocumentSettings>
                      <o:PixelsPerInch>96</o:PixelsPerInch>
                    </o:OfficeDocumentSettings>
                  </xml>
                </noscript>
                <![endif]-->
                <style>
                  table, td, div, h1, p {font-family: Arial, sans-serif;}
                </style>
              </head>
              <body style='margin:0;padding:0;text-align:center'>
                <p>Please click the button to verify your SPM:<br>username: $username <br>email: $email <br>password: $password</p>
                <br>
                <div><a target='_blank'  style='background-color: #199319;
              
                          color: white;
              
                          padding: 15px 25px;
              
                          text-decoration: none;
              
                          border-radius: 5px;' href='http://localhost/systematic-planner-management/authentication/confirm.php?email=$email&token=$token'>Verify Email</a></div><br><br>
              </body>
              </html>";
              $mail->msgHTML($content); // email body in HTML format


              // Send the email and check for errors
              if ($mail->send()) {
                $_SESSION['message'] = "You have been registered! Please verify your email! Check Spam if not in Inbox.";
              } else {
                echo 'Error: ' . $mail->ErrorInfo;
              }
            }

            // $_SESSION['message'] = "You are now logged in";
            // $_SESSION['username'] = $username;
            // header("location: ../index.php");  //redirect home page
          } else {
            $_SESSION['message'] = "The two password do not match";
          }
        }
      }
    }
  }
}
?>

<?php

include "../components/header.php";

?>

<main class="main-content">



  <?php
  if (isset($_SESSION['message'])) {
    echo "<div class='container alert alert-warning'>" . $_SESSION['message'] . "</div>";
    unset($_SESSION['message']);
  }
  ?>

  <!-- Sign Up page -->
  <div style="display:flex; justify-content: center; flex-direction: column; align-items: center;">
    <br>

    <form method="post" action="register.php">

      <p><strong>Username:</strong></p>
      <input required type="text" name="username" class="textInput form-control" style="margin-bottom: 10px; margin-top: -10px;" placeholder="Enter username">

      <p><strong>Email:</strong></p>
      <input required type="email" name="email" class="textInput form-control" style="margin-bottom: 10px; margin-top: -10px;" placeholder="Enter email">


      <p><strong>Password:</strong></p>
      <input required type="password" name="password" class="textInput form-control" style="margin-bottom: 10px; margin-top: -10px;" placeholder="Enter password">

      <p><strong>Confirm Password:</strong></p>
      <input required type="password" name="password2" class="textInput form-control" style="margin-bottom: 15px; margin-top: -10px;" placeholder="Enter password again">


      <div style="text-align: center;">
        <button class="btn btn-primary form-control" type="submit" name="register_btn" style=" margin-top: 10px">Sign up</button>
      </div>
      <div class="mt-3"><a href="login.php">Already have an account? click here.</a></div>

    </form>
  </div>

</main>
</div>
</div>
<?php

include "../components/footer.php";

?>

</body>

</html>