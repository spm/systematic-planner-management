<?php if (basename($_SERVER['PHP_SELF']) == 'auth.php') {
    header("location: index.php");
} ?>

<!-- Validating if a user is signed in or not -->
<?php
session_start();

if (basename($_SERVER['PHP_SELF']) == 'index.php') {
    include "database/db.php";
} else {
    include "../database/db.php";
}



if (!$_SESSION['username']) {
    if (basename($_SERVER['PHP_SELF']) == 'index.php') {
        header("location: authentication/login.php");
        exit();
    } else {
        header("location: ../authentication/login.php");
        exit();
    }
}

// Current User

$usern = $_SESSION['username'];

?>